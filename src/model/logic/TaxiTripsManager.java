package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import com.google.gson.*;

import api.ITaxiTripsManager;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.ILinkedList;
import model.data_structures.LinkedList;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private LinkedList<Service> services;
	private LinkedList<Taxi> taxis;
	
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		BufferedReader reader = null;
		
		try
		{			
			JsonParser parser = new JsonParser();

				JsonArray arr = (JsonArray) parser.parse(new FileReader(serviceFile));
				services = new LinkedList<Service>();
				taxis = new LinkedList<Taxi>();
				for (int i = 0; arr != null && i < arr.size(); i++)
				{
					JsonObject obj = (JsonObject) arr.get(i);
					
					double costoTotal = 0.0;
					String id = "";
					int tiempoSegundos = 0;
					double distancia = 0.0;
					String taxi_id = "";
					String company = "";
					int dropoff_community_area = 0;
					int pickup_community_area = 0;
					
					if(obj.get("trip_total")!= null)
						costoTotal = obj.get("trip_total").getAsDouble();
					
					if(obj.get("trip_id")!= null)
						id = obj.get("trip_id").getAsString();
					
					if(obj.get("trip_seconds")!= null)
						tiempoSegundos = obj.get("trip_seconds").getAsInt();
					
					if(obj.get("trip_miles")!= null)
						distancia = obj.get("trip_miles").getAsDouble();
					
					if(obj.get("taxi_id") != null)
						taxi_id = obj.get("taxi_id").getAsString();
					
					if(obj.get("company") != null)
						company = obj.get("company").getAsString();
					
					if(obj.get("dropoff_community_area") != null)
						dropoff_community_area = obj.get("dropoff_community_area").getAsInt();
					
					if(obj.get("pickup_community_area") != null)
						pickup_community_area = obj.get("pickup_community_area").getAsInt();
					
					Service nuevo = new Service(id, tiempoSegundos, distancia, costoTotal, dropoff_community_area, pickup_community_area, taxi_id, company);		
					services.add(nuevo);
					
					Taxi taxi =  new Taxi (taxi_id,company);
					if(!taxis.contains(taxi))
						taxis.add(taxi);
				}
				
		}
		
		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
			
		
		
		System.out.println("Inside loadServices with " + serviceFile);
	}

	@Override
	public ILinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxisOfCompany with " + company);
		LinkedList<Taxi> taxisCompany = new LinkedList<Taxi>();
		int i = 0;
		while(i < taxis.size())
		{
			if(taxis.get(i).getCompany().equalsIgnoreCase(company))
				taxisCompany.add(taxis.get(i));
			i++;
		}
		
		return taxisCompany;
	}

	
	@Override
	public ILinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		LinkedList<Service> servicesArea = new LinkedList<Service>();
		int i = 0;
		while(i < services.size())
		{
			if(services.get(i).getDropoffCommunityArea() == communityArea)
				servicesArea.add(services.get(i));
			i++;
		}
		
		return servicesArea;
	}


}
