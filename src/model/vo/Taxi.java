package model.vo;

import model.data_structures.LinkedList;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	/**
	 * Id del taxi.
	 */
	private String taxiId;
	
	/**
	 * Compa��a a la que pertenece el taxi.
	 */
	private String company;	
	
	/**
	 * Crea el taxi con los valores dados por par�metro.
	 * @param id Id del taxi. id != null && id != "".
	 * @param pCompany Compa�ia del taxi. pCompany != null && pCompany != "".
	 */
	public Taxi(String id, String pCompany)
	{
		taxiId = id;
		company = pCompany;
	}
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return  taxiId;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		if (this.getTaxiId().compareTo(o.getTaxiId()) < 0)
			return -1;
		else if (this.getTaxiId().compareTo(o.getTaxiId()) > 0)
			return 1;
		else
			return 0;
	}	
}
