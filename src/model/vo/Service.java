package model.vo;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	/**
	 * Compa�ia del taxi del servicio.
	 */
	private String company;
	
	/**
	 * Identificaci�n del servicio.
	 */
	private String tripId;
	
	/**
	 * Costo total del servicio.
	 */
	private double tripTotal;
	
	/**
	 * Tiempo en segundos del servicio.
	 */
	private int tripSeconds;
	
	/**
	 * Identificaic�n del taxi.
	 */
	private String taxiId;
	
	/**
	 * Distancia en millas del servicio.
	 */
	private double tripMiles;
	
	/**
	 * Area donde termina el servicio.
	 */
	private int dropoffCommunityArea;
	
	/**
	 * Area donde inicia el servicio.
	 */
	private int pickupCommunityArea;
	
	
	/**
	 * Crea el servicio con los valores dados por par�metro.
	 * @param id Identificaci�n del servicio. id != null && id != "".
	 * @param tiempoS Duraci�n del servicio. tiempoS != null && tiempo >= 0.
	 * @param ditancia Distancia del servicio. distancia != null && distancia >= 0.
	 * @param costo Costo total del servicio. costo != null && costo >= 0.
	 * @param dropoffArea Area donde termina el servicio. dropoffArea != null.
	 * @param pickupArea Area donde incia el servicio. pickupArea != null.
	 * @param pTaxiId Identificaci�n del taxi. pTaxiId != null && pTaxiId != "".
	 * @param pCompany Compa�ia del taxi. pCompany != null && pCompany != null.
	 */
	public Service(String id, int tiempoS, double ditancia, double costo, int dropoffArea, int pickupArea, String pTaxiId, String pCompany)
	{
		tripId = id;
		tripSeconds = tiempoS;
		tripMiles = ditancia;
		tripTotal = costo;
		dropoffCommunityArea = dropoffArea;
		taxiId = pTaxiId;
		company = pCompany;
		pickupCommunityArea = pickupArea;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		if(this.getTripId().compareTo(o.getTripId()) > 0)
			return 1;
		else if(this.getTripId().compareTo(o.getTripId()) < 0)
			return -1;
		else
			return 0;
	}
	
	public int getDropoffCommunityArea()
	{
		return dropoffCommunityArea;
	}
	
	public int getPickupCommunityArea()
	{
		return pickupCommunityArea;
	}
}
