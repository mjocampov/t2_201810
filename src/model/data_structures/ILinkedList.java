package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add: add a new element T 
 * delete: delete the given element T 
 * get: get the given element T (null if it doesn't exist in the list)
 * size: return the the number of elements
 * get: get an element T by position (the first position has the value 0) 
 * listing: set the listing of elements at the firt element
 * getCurrent: return the current element T in the listing (return null if it doesn�t exists)
 * next: advance to next element in the listing (return if it exists)
 * @param <T>
 */
public interface ILinkedList <T extends Comparable<T>>  
{
	/**
	 * Agrega un elemento a la lista.
	 * @param element Elemento que se va a agregar. element != null.
	 */
	public void add(T element);
	
	/**
	 * Elimina un elemento de la lista.
	 * @param element Elemento que se quiere eliminar. element != null.
	 */
	public void delete(T element);
	
	/**
	 * Busca el elemento dado por par�metro en la lista.
	 * @param element Elemento buscado. element != null.
	 * @return El elemento si lo encontr� y null de lo contrario.
	 */
	public T get(T element);
	
	/**
	 * Retorna el tama�o de la lista.
	 * @return El tama�o de la lista.
	 */
	public int size();
	
	/**
	 * Retorna el elemento en la posici�n dada por par�metro.
	 * @param position Posici�n del elemento. position != null && position > 0.
	 * @return El elemento en esa posici�n si existe y null de lo contrario.
	 */
	public T get(int position);
	
	/**
	 * Modifica el elemeto actual por el primero.
	 */
	public void listing();
	
	/**
	 * Retorna el elemento actual de la lista.
	 * @return El elemento actual de a lista.
	 */
	public T getCurrent();
	
	/**
	 * Retorna el siguiente elemento de la lista.
	 * @return El siguiente elemento de la lista.
	 */
	public T next();
	
	/**
	 * Retorna si el elemento dado por par�metro pertenece o no a lista.
	 * @param element Elemento buscado. element != null.
	 * @return True si pertence y false de lo contrario.
	 */
	public boolean contains(T element);
}
