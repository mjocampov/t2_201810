package model.data_structures;

import model.data_structures.Node;
/**
 * 
 * @author majoc
 *
 * @param <T>
 */
public class LinkedList<T extends Comparable<T>> implements ILinkedList<T> 
{
	/**
	 * Primer nodo de la lista.
	 */
	private Node<T> first;

	/**
	 * Nodo actual de la lista.
	 */
	private Node<T> current;

	/**
	 * Tama�o de la lista.
	 */
	private int size;

	/**
	 * Crea la lista con el primer nodo.
	 * @param pFirst Primer nodo de la lista. pFirst != null.
	 */
	public LinkedList(T pFirst)
	{
		first = new Node<T>(pFirst);
		current = first;
		size = 1;
	}

	/**
	 * Crea la lista vac�a.
	 */
	public LinkedList() 
	{
		first = null;
		current = null;
		size = 0;
	}

	@Override
	public void add(T element) 
	{
		if(element == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		} 

		else
		{
			if(first == null)
			{
				first = new Node<T>(element);
				current = first;
				size ++;
			}

			else
			{
				current.setNext(element);
				current = current.getNext();
				size++;

			}
		}
	}

	@Override
	public void delete(T element) 
	{
		if(first != null)
		{
			if(first.getElement().compareTo(element)==0)
			{
				first = first.getNext();
				size--;
			}

			else
			{
				Node<T> anterior = first;
				Node<T> actual = anterior.getNext();

				while(actual != null)
				{					
					if(actual.getElement().compareTo(element) == 0)
					{
						current = anterior;
						anterior.setNext(actual.getNext().getElement());
						size--;
					}

					anterior = actual;
					actual = actual.getNext();
				}
			}
		}

	}

	@Override
	public T get(T element) 
	{
		T buscado = null;

		if(first != null)
		{
			Node<T> actual = first;

			while(actual != null)
			{
				if(actual.getElement().compareTo(element) == 0)
				{
					buscado = actual.getElement();
				}

				actual = actual.getNext();
			}
		}

		return buscado;

	}

	@Override
	public int size() 
	{
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T get(int position) 
	{
		T buscado = null;

		if(first != null)
		{
			Node<T> actual = first;
			int pos = 0;
			while(actual != null && pos <= position)
			{
				if(pos == position)
				{
					buscado = actual.getElement();
				}

				pos++;
				actual = actual.getNext();
			}
		}

		return buscado;


	}

	@Override
	public void listing() 
	{
		// TODO Auto-generated method stub
		current = first;
	}

	@Override
	public T getCurrent() 
	{
		// TODO Auto-generated method stub
		if(current != null)
			return current.getElement();
		else
			return null;
	}

	@Override
	public T next() 
	{
		// TODO Auto-generated method stub
		if(current.getNext() != null)
			return current.getNext().getElement();
		else
			return null;
	}

	@Override
	public boolean contains(T element) {
		
		boolean contain = false;
		
		if(first != null)
		{
			Node<T> actual = first;

			while(actual != null && !contain)
			{
				if(actual.getElement().compareTo(element) == 0)
				{
					contain = true;
				}

				actual = actual.getNext();
			}
		}
		
		return contain;
	}

}
