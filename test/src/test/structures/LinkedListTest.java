package test.structures;

import junit.framework.TestCase;
import model.data_structures.LinkedList;

public class LinkedListTest extends TestCase
{
	
	private LinkedList<Integer> lista;
	
	/**
	 * Prepara el escenario de pruebas
	 */
	public void setUp()
	{
		lista = new LinkedList<Integer>();
	}
	
	/**
	 * Prueba agregando un elemento.<br>
	 * Casos de prueba:<br>
	 * 1. Agrega un elemento y comprueba que este en la lista.<br>
	 * 2. Agrega un elemento y comprueba el tama�o de la lista.
	 */
	public void testAgregar()
	{
		setUp();
		lista.add(10);
		assertTrue("El elemento no fue agregado", lista.get(0) == 10);
		assertTrue("El elemento no fue agregado", lista.get(new Integer(10)) == 10);
	}
	
	/**
	 * Prueba eliminar un elemento.<br>
	 * Casos de prueba:<br>
	 * 1. Elimina el elemento y despu�s lo busca.
	 */
	public void testEliminar()
	{
		setUp();
		lista.add(10);
		lista.delete(10);
		assertNull("El elemento no fue eliminado", lista.get(new Integer(10)));
	}
	
	/**
	 * Prueba buscar el elemento.<br>
	 * Casos de prueba:<br>
	 * 1. Busca varios elementos que est�n en la lista.<br>
	 * 2. Busca un elemento que no est� en la lista.
	 */
	public void testGetElemento()
	{
		setUp();
		lista.add(10);
		lista.add(15);
		lista.add(28);
		assertTrue("El elemento no fue encontrado", lista.get(new Integer(15)) == 15);
		assertTrue("El elemento no fue encontrado", lista.get(new Integer(10)) == 10);
		assertTrue("El elemento no fue encontrado", lista.get(new Integer(28)) == 28);
		assertNull("El elemento no deber�a ser encontrado", lista.get(new Integer(32)));
	}
	
	/**
	 * Prueba buscar un elemento por posici�n.<br>
	 * Casos de prueba: <br>
	 * 1. Busca varios elementos que si est�n en la lista. <br>
	 * 2. Busca varios elementos que no est�n en la lista. 
	 */
	public void testGetPosicion()
	{
		setUp();
		lista.add(10);
		lista.add(15);
		lista.add(28);
		assertTrue("El elemento no fue encontrado", lista.get(0) == 10);
		assertTrue("El elemento no fue encontrado", lista.get(1) == 15);
		assertTrue("El elemento no fue encontrado", lista.get(2) == 28);	
		assertNull("El elemento no deber�a ser encontrado", lista.get(3));
		assertNull("El elemento no deber�a ser encontrado", lista.get(4));
	}
	
	/**
	 * Prueba actualizar el elemento actual. <br>
	 * Casos de prueba:<br>
	 * 1. Se agregan varios elementos y luego se modifica el actual por el primero.
	 */
	public void testListing()
	{
		setUp();
		lista.add(10);
		lista.add(15);
		lista.add(28);
		lista.listing();
		assertTrue("El elemento actual no es el primero", lista.getCurrent() == 10);
	}
	
	/**
	 * Prueba el siguiente elemento. <br>
	 * Casos de prueba: <br>
	 * 1. Pide el siguiente del �ltimo elemento. <br>
	 * 2. Pide ele siguiente del primer elemento. 
	 */
	public void testNext()
	{
		setUp();
		lista.add(10);
		lista.add(15);
		lista.add(28);
		
		assertNull("No deber�a retornar un elemento", lista.next());
		
		lista.listing();
		assertTrue("Deber�a retornar el segundo elemento", lista.next() == 15);
	}
	
	/**
	 * Prueba que el tama�o de la lista se est� actualizando.<br>
	 * Casos de prueba:<br>
	 * 1. Agrega varios elementos.<br>
	 * 2. Elimina un elemento.
	 */
	public void testSize()
	{
		setUp();
		lista.add(10);
		lista.add(15);
		lista.add(28);
		assertTrue("El tama�ano no es el esperado", lista.size() == 3);
		
		lista.delete(15);
		assertTrue("El tama�ano no es el esperado", lista.size() == 2);
		
	}
}
